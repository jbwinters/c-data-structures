INC=./include
SRC=./src
BUILD=./build

DEBUG ?= 1
ifeq ($(DEBUG), 1)
        CFLAGS = -g -Wall -Werror -DDEBUG -O0
else
        CFLAGS = -DNDEBUG -O2
endif

CC = g++ $(CFLAGS) -I$(INC)

MKDIR_P = mkdir -p

clean = $(BUILD)
objects = linked_list

.PHONY: tests clean directories


all: clean directories $(objects)

directories: $(BUILD)

$(BUILD):
	$(MKDIR_P) $(BUILD)

linked_list: $(SRC)/linked_list.cc
	$(CC) -c $^ -o $(BUILD)/linked_list.o

tests: all
	cd tests/; ./build_and_run_tests.sh

clean:
	rm -rf $(clean)

cleanall: clean
	rm -rf tests/build

