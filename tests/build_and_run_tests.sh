#!/bin/bash
#rm -rf build
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=DEBUG ..
make

if [ $? -ne 0 ]; then
    exit $?
fi


echo ========================
echo '    Running Tests     '
echo ========================

for f in ./bin/*
do
    $f
done
