#include "linked_list.h"
#include "exceptions.h"
#include <gtest/gtest.h>

class LeakChecker : public ::testing::EmptyTestEventListener {
 private:
  // Called before a test starts.
  virtual void OnTestStart(const testing::TestInfo& t/* test_info */) {
    initially_allocated_ = Node::allocated();
  }

  // Called after a test ends.
  virtual void OnTestEnd(const testing::TestInfo& t/* test_info */) {
    int difference = Node::allocated() - initially_allocated_;

    // You can generate a failure in any event handler except
    // OnTestPartResult. Just use an appropriate Google Test assertion to do
    // it.
    EXPECT_LE(difference, 0) << "Leaked " << difference << " nodes.";
  }

  int initially_allocated_;
};

TEST(LinkedList, Constructor) {
    const LinkedList li;
    EXPECT_EQ(0, li.length());
    EXPECT_TRUE(li.is_empty());
}

TEST(LinkedList, IsEmpty) {
    LinkedList li;
    EXPECT_TRUE(li.is_empty());

    li.insert(5);
    EXPECT_FALSE(li.is_empty());
}

TEST(LinkedList, Length){
    LinkedList li;
    EXPECT_EQ(0, li.length());

    li.insert(1);
    EXPECT_EQ(1, li.length());

    li.insert(1);
    li.insert(1);
    EXPECT_EQ(3, li.length());
}

TEST(LinkedList, At) {
    LinkedList li;
    EXPECT_THROW(li.at(0), ParameterError);

    li.insert(5);
    EXPECT_EQ(5,li.at(0));

    li.insert(6);
    EXPECT_EQ(6,li.at(0));
    EXPECT_EQ(5,li.at(1));
}

TEST(LinkedList, Insert){
    LinkedList li;
    EXPECT_THROW(li.insert(5, 7), ParameterError);
    EXPECT_EQ(0, li.length());

    EXPECT_NO_THROW(li.insert(5, 0));
    EXPECT_EQ(1, li.length());
    EXPECT_EQ(5,li.at(0));

    EXPECT_NO_THROW(li.insert(6));
    EXPECT_EQ(2, li.length());
    EXPECT_EQ(6,li.at(0));

    EXPECT_NO_THROW(li.insert(7,2));
    EXPECT_EQ(3, li.length());
    EXPECT_EQ(7,li.at(2));

    li.insert(8,1);
    EXPECT_EQ(8,li.at(1));

}

TEST(LinkedList, Remove){
    LinkedList li;
    EXPECT_THROW(li.remove(), ParameterError);
    li.insert(0);
    EXPECT_EQ(0, li.remove());
    EXPECT_EQ(0, li.length());

    li.insert(1);
    li.insert(2, li.length());
    li.insert(3, li.length());
    EXPECT_EQ(1, li.remove());

    li.insert(1);
    EXPECT_EQ(2, li.remove(1));

    li.insert(2, 1);
    EXPECT_EQ(3, li.remove(2));

    LinkedListIter* lli = li.create_iterator();
    int i=1;
    for (lli->first(); !lli->is_done(); lli->next(), i++){
        EXPECT_EQ(i, lli->current_value());
    }
    EXPECT_EQ(3, i);
}

TEST(LinkedList, Clear){
    LinkedList li;
    for (int i=0; i < 10; i++){
        li.insert(i, li.length());
    }
    EXPECT_EQ(10, li.length());
    li.clear();
    EXPECT_EQ(0, li.length());

}

TEST(LinkedList, Find){
    LinkedList li;

    // list is empty
    EXPECT_EQ(-1, li.find(7));

    // value does not exist
    li.insert(4);
    li.insert(6);
    EXPECT_EQ(-1, li.find(5));
    li.insert(5);

    // value exists at beginning
    EXPECT_EQ(0, li.find(5));

    // value exists at end
    EXPECT_EQ(2, li.find(4));

    // value exists in middle
    EXPECT_EQ(1, li.find(6));
}

TEST(LinkedList, Iterator){
    LinkedList li;
    for (int i=0; i < 10; i++){
        li.insert(i, li.length());
    }
    LinkedListIter* lli = li.create_iterator();
    int j = 0;
    for (lli->first(); !lli->is_done(); lli->next(), j++){
        EXPECT_EQ(j, lli->current_value());
    }
    EXPECT_EQ(10, j);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    testing::TestEventListeners& listeners =
        testing::UnitTest::GetInstance()->listeners();
    listeners.Append(new LeakChecker);
    return RUN_ALL_TESTS();
}
