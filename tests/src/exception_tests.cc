#include "exceptions.h"
#include <gtest/gtest.h>
#include <iostream>

TEST(Error, What) {
    Error error1;
    EXPECT_STREQ("An error occurred.", error1.what());

    Error error2("A specific error occurred.");
    EXPECT_STREQ("A specific error occurred.", error2.what());
}

TEST(ParameterError, What) {
    ParameterError error1;
    EXPECT_STREQ("An error occurred.", error1.what());

    ParameterError error2("A specific error occurred.");
    EXPECT_STREQ("A specific error occurred.", error2.what());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
