#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_
#include <cstddef>

struct Node{
    int value;
    Node* next;

    Node(int v);
    Node(Node& n);

    //#ifdef DEBUG
    void* operator new(size_t allocation_size);
    void operator delete(void* block, size_t t);
    static int allocated();
    private:
    static int allocated_;
    //#endif
};

class LinkedListIter;

class LinkedList
{
    protected:
        Node* head;
        Node* tail;
        size_t length_;
        Node* node_at(size_t index) const;
    public:
        LinkedList();
        ~LinkedList();
        size_t length() const;
        bool is_empty() const;

        // get value at zero-based index
        int at(size_t index) const;

        // insert node at zero-based index n
        void insert(int value, size_t index=0);

        // remove node at zero-based index
        int remove(size_t index=0);

        // clear list
        void clear();

        // find index of first occurrence of value
        // returns -1 if value is not in the list
        int find(int value) const;

        // iterator
        LinkedListIter* create_iterator() const;
        friend class LinkedListIter;

};

class LinkedListIter
{
    protected:
        const LinkedList* list;
        Node* cur_node;
    public:
        LinkedListIter(const LinkedList* li);
        void first();
        void next();
        bool is_done() const;
        int current_value() const;
};

#endif /* _LINKED_LIST_H_ */
