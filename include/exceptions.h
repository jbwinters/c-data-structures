#ifndef _EXCEPTIONS_H_
#define _EXCEPTIONS_H_

#include <exception>
#include <cstring>

class Error: public std::exception
{
    public:
        Error()
            : what_str(0)
        {
            what_str = new char[20];
            strcpy(what_str, "An error occurred.");
        }
        ~Error() throw()
        {
            delete what_str;
        }

        Error(const char* description)
            : what_str(0)
        {
            what_str = new char[strlen(description) + 1];
            strcpy(what_str, description);
        }

        virtual const char* what() const throw()
        {
            return strdup(what_str);
        }
    protected:
        char* what_str;
};

class ParameterError : public Error
{
    public:
        ParameterError(): Error() {}
        ParameterError(const char* description): Error(description) {}
};

#endif /* _EXCEPTIONS_H_ */
