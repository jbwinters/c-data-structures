#include "linked_list.h"
#include "exceptions.h"

#ifdef DEBUG
#include <stdlib.h>
#include <iostream>
using std::cout;
using std::endl;
#endif

/* Node definition */
Node::Node(int v): value(v), next(0) {}
Node::Node(Node& n): value(n.value), next(0) {}

#ifdef DEBUG
void* Node::operator new(size_t allocation_size){
    allocated_++;
    return malloc(allocation_size);
}
void Node::operator delete(void* block, size_t t){
    allocated_--;
    free(block);
}
int Node::allocated() {return allocated_;}

int Node::allocated_ = 0;
#endif


/* LinkedList definition */
LinkedList::LinkedList()
    : head(0), tail(0), length_(0)
{

}

LinkedList::~LinkedList()
{
    clear();
}

size_t LinkedList::length() const
{
    return length_;
}

bool LinkedList::is_empty() const
{
    return length_ == 0;
}

int LinkedList::at(size_t index) const
{
    if (index + 1 > length_){
        throw ParameterError("index is invalid.");
    }

    Node* const p = node_at(index);
    return p->value;
}

void LinkedList::insert(int value, size_t index)
{
    if (index > length_){
        throw ParameterError("index is invalid.");
    }

    Node* new_node = new Node(value);
    if (length_ != 0 && index != 0){
        Node* p = node_at(index - 1);
        new_node->next = p->next;
        p->next = new_node;
    }

    if (index == 0) {
        new_node->next = head;
        head = new_node;
    }
    if ((size_t)index == length_) {
        tail = new_node;
        new_node->next = 0;
    }

    length_ += 1;
}

int LinkedList::remove(size_t index)
{
    if (index + 1 > length_){
        throw ParameterError("index is invalid.");
    }
    Node* to_destroy = 0;
    Node* prev = 0;
    if (index == 0){
        to_destroy = head;
        head = to_destroy->next;
    }
    else{
        prev = node_at(index - 1);
        to_destroy = prev->next;
        prev->next = to_destroy->next;
    }
    if (to_destroy->next == 0){
        tail = prev;
    }

    length_ -= 1;
    int ret = to_destroy->value;
    delete to_destroy;
    return ret;
}

void LinkedList::clear()
{
    while (head != 0) {
        remove(0);
    }
}

int LinkedList::find(int value) const
{
    if (is_empty()) return -1;
    Node* p = head;
    size_t index = 0;
    while (p != 0){
        if (p->value == value) break;
        p = p->next;
        index++;
    }
    return (index == length_)? -1 : (int)index;
}

LinkedListIter* LinkedList::create_iterator() const
{
    return new LinkedListIter(this);
}

// protected methods

Node* LinkedList::node_at(size_t index) const
{
    Node* p = head;
    for (size_t i=0; i < index; i++) {
        p = p->next;
    }
    return p;
}

/* LinkedListIter definition */

LinkedListIter::LinkedListIter(const LinkedList* li)
    : list(li), cur_node(0)
{}

void LinkedListIter::first()
{
    cur_node = list->head;
}

void LinkedListIter::next()
{
    cur_node = cur_node->next;
}

bool LinkedListIter::is_done() const
{
    return cur_node == 0;
}

int LinkedListIter::current_value() const
{
    return cur_node->value;
}
